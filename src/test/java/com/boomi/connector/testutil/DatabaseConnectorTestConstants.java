// Copyright (c) 2024 Boomi, Inc.
package com.boomi.connector.testutil;

public final class DatabaseConnectorTestConstants {

    /** Assertion error message for operation message mismatch **/
    public static final String OPERATION_MESSAGE_MISMATCH = "Operation message mismatch";
    /** Assertion error message for response status code mismatch **/
    public static final String STATUS_CODE_MISMATCH = "Operation status code mismatch";
    /** Assertion error message for payload metadata mismatch */
    public static final String PAYLOAD_METADATA_ASSERT_FAILED = "Payload metadata assert failed";
    /** Assertion error message for payload mismatch */
    public static final String PAYLOAD_ASSERT_FAILED = "Payload assert failed";

    private DatabaseConnectorTestConstants() {

    }
}
