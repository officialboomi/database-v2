// Copyright (c) 2025 Boomi, LP
package com.boomi.connector.databaseconnector.operations.upsert;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.testutil.DataTypesUtil;
import com.boomi.connector.testutil.SimpleTrackedData;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;


/**
 * This Class is used to test {@link CommonUpsert Class}
 */
public class CommonUpsertTest {

    private static final String CATALOG = "catalog";
    private final Connection _connection = Mockito.mock(Connection.class);
    private final ResultSet _resultSetMetaExtractor = Mockito.mock(ResultSet.class);
    private final DatabaseMetaData _databaseMetaData = Mockito.mock(DatabaseMetaData.class);
    private final PreparedStatement _preparedStatement = Mockito.mock(PreparedStatement.class);
    private Method method;
    private final Class[] parameterTypes = new Class[3];
    private final Object[] parameters = new Object[3];

    @Before
    public void setup() throws SQLException {
        Mockito.when(_connection.getMetaData()).thenReturn(_databaseMetaData);
        Mockito.when(_connection.getCatalog()).thenReturn(CATALOG);
        Mockito.when(_databaseMetaData.getColumns(CATALOG, "EVENT", CATALOG, null)).thenReturn(_resultSetMetaExtractor);

        parameterTypes[0] = PreparedStatement.class;
        parameterTypes[1] = ObjectData.class;
        parameterTypes[2] = Map.class;
    }

    /**
     * Verifies that null values in input JSON are correctly set as NULL parameters
     * in the PreparedStatement during upsert operation.
     *
     * @throws SQLException              if database access fails
     * @throws NoSuchMethodException     if method not found
     * @throws InvocationTargetException if method invocation fails
     * @throws IllegalAccessException    if method access denied
     */
    @Test
    @java.lang.SuppressWarnings("java:S3011")
    public void testNullSetWhenPassingInputAsNull()
            throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Map<String, String> dataTypeMap = DataTypesUtil.getDataTypeMap();

        SimpleTrackedData trackedData = new SimpleTrackedData(13,
                new ByteArrayInputStream(DataTypesUtil.INPUT_JSON_NULL_VALUE.getBytes(StandardCharsets.UTF_8)));

        CommonUpsert commonUpsert = new CommonUpsert(_connection, 1L, CATALOG, "Schema Name", "EVENT", false,
                DataTypesUtil.getLinkedHashSet());

        parameters[0] = _preparedStatement;
        parameters[1] = trackedData;
        parameters[2] = dataTypeMap;

        method = commonUpsert.getClass().getDeclaredMethod("appendInsertParams", parameterTypes);
        method.setAccessible(true);
        method.invoke(commonUpsert, parameters);

        boolean testResult = DataTypesUtil.verifyTestForNullSetExecute(_preparedStatement);
        Assert.assertTrue(testResult);
    }
}
