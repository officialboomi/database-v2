{
    "requestBody": {
        "entries": [
            {
				"id":"123",
				"name":"abc",
				"day":"2019-12-09",
				"clob":"Hello",
				"isqualified":true,
				"clock":"21:09:08"
				
            },
             {
				"id":"120",
				"name":"xyz",
				"day":"2020-12-26",
				"clob":"Hello",
				"isqualified":false,
				"clock":"23:58:40"
            }
        ]
    }
}